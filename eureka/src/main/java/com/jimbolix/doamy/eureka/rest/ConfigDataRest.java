package com.jimbolix.doamy.eureka.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/1 09:33
 * @Description: 
 */
@RestController
@RequestMapping("/dataset")
@Slf4j
public class ConfigDataRest {

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/all/services")
    public List<String> detail(){
        log.info("查看所有已注册服务");
        List<String> services = discoveryClient.getServices();
        log.info("查看所有已注册服务结束");
        return services;
    }

}