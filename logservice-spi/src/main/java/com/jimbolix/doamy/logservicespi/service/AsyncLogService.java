package com.jimbolix.doamy.logservicespi.service;

import com.jimbolix.doamy.logservicespi.client.LogServiceClient;
import com.jimbolix.doamy.logservicespi.model.dto.SysOperLogDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/8 20:42
 * @Description:
 */
@Service
@Slf4j
public class AsyncLogService {
    @Autowired
    private LogServiceClient logServiceClient;

    @Async
    public void saveLog(SysOperLogDTO sysOperLogDTO) {
        try {
            TimeUnit.SECONDS.sleep(20);
            log.info("休息20秒后开始保存日志");
            logServiceClient.save(sysOperLogDTO);
        } catch (Exception e) {
            log.info("保存日志发生异常", e);
        }
    }
}