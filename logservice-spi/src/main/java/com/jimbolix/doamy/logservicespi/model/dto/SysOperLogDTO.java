package com.jimbolix.doamy.logservicespi.model.dto;
/**
 * @Author: ruihui.li
 * @Date: 2020/11/8 20:21
 * @Description: 
 */
public class SysOperLogDTO {

    private String ip;
    private String detail;
    private String operator;
    private String operParam;
    //操作结果
    private boolean result;
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOperParam() {
        return operParam;
    }

    public void setOperParam(String operParam) {
        this.operParam = operParam;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}