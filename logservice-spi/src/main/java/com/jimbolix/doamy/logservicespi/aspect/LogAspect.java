package com.jimbolix.doamy.logservicespi.aspect;

import com.jimbolix.doamy.logservicespi.annotation.DoamyLog;
import com.jimbolix.doamy.logservicespi.model.dto.SysOperLogDTO;
import com.jimbolix.doamy.logservicespi.service.AsyncLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/4 22:12
 * @Description: 日志记录切面
 */
@Aspect
@Slf4j
@Component
public class LogAspect {

    @Autowired
    private AsyncLogService asyncLogService;

    @Pointcut("@annotation(com.jimbolix.doamy.logservicespi.annotation.DoamyLog)")
    public void logPointCut(){}

    @AfterReturning(pointcut = "logPointCut()",returning = "result")
    public void logSuccess(JoinPoint joinPoint,Object result){
        handleLog(joinPoint,null,result);
        log.info("返回通知执行");
    }

    @AfterThrowing(pointcut = "logPointCut()",throwing = "e")
    public void logError(JoinPoint joinPoint,Exception e){
        handleLog(joinPoint,e,null);
        log.error("异常通知执行，异常信息{}",e);
    }

    private void handleLog(final JoinPoint joinPoint,final Exception e,Object result){
        try{
            DoamyLog annotation = getAnnotation(joinPoint);
            if(null == annotation){
                return;
            }
            //操作内容
            String describe = annotation.describe();
            SysOperLogDTO sysOperLogDTO = new SysOperLogDTO();
            sysOperLogDTO.setDetail(describe);
            sysOperLogDTO.setResult(true);
            asyncLogService.saveLog(sysOperLogDTO);
            if(null != e){
                sysOperLogDTO.setResult(false);
            }
        }catch (Exception ex){
            log.error("记录日志发生异常",ex);
        }
    }

    private DoamyLog getAnnotation(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        if(null != method){
            return method.getAnnotation(DoamyLog.class);
        }
        return null;
    }
}