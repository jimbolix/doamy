package com.jimbolix.doamy.logservicespi.client;

import com.jimbolix.doamy.logservicespi.rest.LogSaveSpi;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/8 21:28
 * @Description: 
 */
@FeignClient("LOGSERVICE")
public interface LogServiceClient extends LogSaveSpi {
}