package com.jimbolix.doamy.logservicespi.annotation;


import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DoamyLog {

    /**
     * 操作内容
     * @return
     */
    String describe();
}
