package com.jimbolix.doamy.logservicespi.rest;

import com.jimbolix.doamy.logservicespi.model.dto.SysOperLogDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/8 20:19
 * @Description: 
 */
@RequestMapping("/log")
public interface LogSaveSpi {

    @PostMapping
    boolean save(SysOperLogDTO sysOperLogDTO);
}