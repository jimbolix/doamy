package com.jimbolix.doamy.userservice.controller;

import com.jimbolix.doamy.common.domain.DoAmyResult;
import com.jimbolix.doamy.logservicespi.annotation.DoamyLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: ruihui.li
 * @Date: 2020/11/8 20:57
 * @Description: 
 */
@RestController
@RequestMapping("/user")
@Slf4j
@ComponentScan(basePackages = {"com.jimbolix.doamy"})
public class UserController {

    @PostMapping
    @DoamyLog(describe = "保存日志信息。。")
    public DoAmyResult<Boolean> save(Map<String,String> userMap){
        log.info("保存用户信息");
        return DoAmyResult.ok(true);
    }
}