package com.jimbolix.doamy.common.domain;

import java.io.Serializable;

/**
 * @Author: ruihui.li
 * @Date: 2020/10/28 23:12
 * @Description: 响应主体
 */
public class DoAmyResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final int SUCCESS_CODE = 200;

    private static final int FAILED_CODE = 500;

    private int code;

    private String msg;

    private T data;

    public static <T> DoAmyResult<T> ok()
    {
        return restResult(null, SUCCESS_CODE, null);
    }

    public static <T> DoAmyResult<T> ok(T data)
    {
        return restResult(data, SUCCESS_CODE, null);
    }

    public static <T> DoAmyResult<T> ok(T data, String msg)
    {
        return restResult(data, SUCCESS_CODE, msg);
    }

    public static <T> DoAmyResult<T> fail()
    {
        return restResult(null, FAILED_CODE, null);
    }

    public static <T> DoAmyResult<T> fail(String msg)
    {
        return restResult(null, FAILED_CODE, msg);
    }

    public static <T> DoAmyResult<T> fail(T data)
    {
        return restResult(data, FAILED_CODE, null);
    }

    public static <T> DoAmyResult<T> fail(T data, String msg)
    {
        return restResult(data, FAILED_CODE, msg);
    }

    public static <T> DoAmyResult<T> fail(int code, String msg)
    {
        return restResult(null, code, msg);
    }

    private static <T> DoAmyResult<T> restResult(T data, int code, String msg)
    {
        DoAmyResult<T> apiResult = new DoAmyResult();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }
}