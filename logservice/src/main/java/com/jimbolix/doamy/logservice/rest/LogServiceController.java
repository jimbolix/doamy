package com.jimbolix.doamy.logservice.rest;

import com.jimbolix.doamy.logservicespi.model.dto.SysOperLogDTO;
import com.jimbolix.doamy.logservicespi.rest.LogSaveSpi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author: ruihui.li
 * @Date: 2020/11/3 22:50
 * @Description:  已注册服务获取
 */
@Slf4j
@RestController
public class LogServiceController implements LogSaveSpi {


    public boolean save(SysOperLogDTO sysOperLogDTO) {
        log.info("接受到操作日志");
        log.info("操作内容{}",sysOperLogDTO.getDetail());
        return true;
    }
}